package l1v

import (
	gomock "github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/util/akitaext"
	"gitlab.com/ujjalbuet/mem"
	"gitlab.com/ujjalbuet/mem/cache"
)

var _ = Describe("Respond Stage", func() {
	var (
		mockCtrl *gomock.Controller
		cache1   *Cache
		topPort  *MockPort
		s        *respondStage
		block    *cache.Block
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		topPort = NewMockPort(mockCtrl)
		cache1 = &Cache{
			TopPort: topPort,
			warpts:  7,
		}
		cache1.TickingComponent = akitaext.NewTickingComponent(
			"cache", nil, 1, cache1)
		s = &respondStage{cache: cache1}
		block = &cache.Block{
			IsValid: true,
			Wts:     6,
			Rts:     15,
		}

	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should do nothing if there is no transaction", func() {
		madeProgress := s.Tick(10)
		Expect(madeProgress).To(BeFalse())
	})

	Context("read", func() {
		var (
			read  *mem.ReadReq
			trans *transaction
		)

		BeforeEach(func() {
			read = mem.ReadReqBuilder{}.
				WithSendTime(5).
				WithAddress(0x100).
				WithPID(1).
				WithByteSize(4).
				Build()

			trans = &transaction{read: read, warpts: 6, block: block}

			cache1.transactions = append(cache1.transactions, trans)
		})

		It("should do nothing if the transaction is not ready", func() {
			madeProgress := s.Tick(10)
			Expect(madeProgress).To(BeFalse())
		})

		It("should stall if cannot send to top", func() {
			trans.data = []byte{1, 2, 3, 4}
			trans.done = true
			topPort.EXPECT().Send(gomock.Any()).Return(&akita.SendError{})

			madeProgress := s.Tick(10)

			Expect(madeProgress).To(BeFalse())
		})

		It("should send data ready to top", func() {
			trans.data = []byte{1, 2, 3, 4}
			trans.done = true
			//trans.block.Wts = 9
			topPort.EXPECT().Send(gomock.Any()).
				Do(func(dr *DataReadyRsp) {
					Expect(dr.RespondTo).To(Equal(read.ID))
					Expect(dr.Data).To(Equal([]byte{1, 2, 3, 4}))
					Expect(cache1.warpts).To(Equal(dr.Wts))
				})

			madeProgress := s.Tick(10)

			Expect(madeProgress).To(BeTrue())
			Expect(cache1.transactions).NotTo(ContainElement((trans)))
			Expect(cache1.warpts).To(Equal(trans.block.Wts))
		})
	})

	Context("write", func() {
		var (
			write *mem.WriteReq
			trans *transaction
		)

		BeforeEach(func() {
			write = mem.WriteReqBuilder{}.
				WithSendTime(5).
				WithAddress(0x100).
				WithPID(1).
				Build()
			trans = &transaction{write: write, block: block}
			cache1.transactions = append(cache1.transactions, trans)
		})

		It("should do nothing if the transaction is not ready", func() {
			madeProgress := s.Tick(10)
			Expect(madeProgress).To(BeFalse())
		})

		It("should stall if cannot send to top", func() {
			trans.done = true
			topPort.EXPECT().Send(gomock.Any()).Return(&akita.SendError{})

			madeProgress := s.Tick(10)

			Expect(madeProgress).To(BeFalse())
		})

		It("should send data ready to top", func() {
			trans.data = []byte{1, 2, 3, 4}
			trans.done = true
			topPort.EXPECT().Send(gomock.Any()).
				Do(func(done *mem.WriteDoneRsp) {
					Expect(done.RespondTo).To(Equal(write.ID))
				})

			madeProgress := s.Tick(10)

			Expect(madeProgress).To(BeTrue())
			Expect(cache1.transactions).NotTo(ContainElement((trans)))
			Expect(cache1.warpts).To(Equal(trans.block.Wts))
		})
	})

})
