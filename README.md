# MGPUSIM

[![Discord](https://img.shields.io/discord/526419346537447424.svg)](https://discord.gg/dQGWq7H) 

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/akita/gcn3)](https://goreportcard.com/report/gitlab.com/akita/gcn3)
[![Test](https://gitlab.com/akita/gcn3/badges/master/pipeline.svg)](https://gitlab.com/akita/gcn3/commits/master)
[![Coverage](https://gitlab.com/akita/gcn3/badges/master/coverage.svg)](https://gitlab.com/akita/gcn3/commits/master)

MGPUSim is a GPU simulator that models AMD GCN3 ISA based GPUs.
